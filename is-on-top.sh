#!/bin/bash -xe

TOKEN="vkPS_-BjxNBnUcNsPn4E"
PROJECT=3591219

TARGET_BRANCH=$(curl -H "PRIVATE-TOKEN: ${TOKEN}" https://gitlab.com/api/v3/projects/${PROJECT}/merge_requests/4002856 | jq -r .target_branch)

echo "target branch is ${TARGET_BRANCH}"

MR_FIRST_COMMIT_SHA=$(curl -H "PRIVATE-TOKEN: ${TOKEN}" https://gitlab.com/api/v3/projects/${PROJECT}/merge_requests/4002856/commits | jq -r ".[0].parent_ids[0]")

BRANCH_LAST_COMMIT_SHA=$(curl -H "PRIVATE-TOKEN: ${TOKEN}" https://gitlab.com/api/v3/projects/${PROJECT}/repository/branches/${TARGET_BRANCH} | jq -r ".commit.id")

if [ "${MR_FIRST_COMMIT_SHA}" != "${BRANCH_LAST_COMMIT_SHA}" ]; then
    echo "Branch is not on top"
    exit 1
fi
